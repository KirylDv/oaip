#include <bits/stdc++.h> //подключает все stl работает только в gnu компиляторе

using namespace std;

int n = 0;

typedef double (*fri)(double, double);

double S(double x, double eps)
{
	double p = 1, k = 1, t = 0;
	n = 1;
	t = p = 1/(1+x);
	while(fabs(p) > eps)
	{
		n++;
		k += k; //*2
		p = pow(x, k-1);
		p = k*p/(1 + p*x);
		t += p;
	}
	return t;
}

double Y(double x)
{
	return 1/(1-x); 
} 

void func(double a, double b, double eps, double h,fri ch)
{
	for(double x = a; x < b + h/2; x += h)
		cout << setw(15) << n << setw(15) << ch(x, eps) << setw(15) << Y(x) << setw(15) << x << '\n';
}

int main()
{
	double a[4];
	cout << "Vvedite a, b, eps, h\n";
	cin >> a[0] >> a[1] >> a[2] >> a[3];
	cout << setw(15) << "N" << setw(15) << "S" << setw(15) << "Y" << setw(15) << "X" << '\n';
	func(a[0], a[1], 2[a], a[3], S); 
	return 0;
}
