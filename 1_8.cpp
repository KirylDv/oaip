#include <iostream>
#include <cmath>

using namespace std;

int main(int argc, char const *argv[])
{
	setlocale(LC_ALL, "Russian");
	double x, y, z, a, b, c;
	printf("%s\n", "Введите x, y, z");
	cin >> x >> y >> z;
	double t = fabs(x - y);
	a = exp(t);
	a *= pow(t, x + y);
	b = atan(x) + atan(z);
	a /= b;
	b = pow( pow(x, 6) + pow(log(y), 2) , 1.0/3);
	c = a + b;
	cout << c;
	return 0;
}
// s = 39.3741.