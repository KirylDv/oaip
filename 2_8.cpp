#include <bits/stdc++.h>

using namespace std;

int main(){
	long double x, y, a;
	int c = 0;
	bool b = 0;
	cout << "Vvedite x, y\n";
	cin >> x >> y;
	while(c < 1 || c > 3){
		if(b)
			cout << "No function\n";
		cout << "Choose function f(x)\n1.sh(x)\n2.x^2\n3.e^x\n";
		cin >> c;
		b = 1;
	}
	switch(c){
		case 1:{
			a = sinh(x);
			break; 
		}
		case 2:{
			a = x*x;
			break;
		}
		case 3:{
			a = exp(x);
			break;
		}
	}
	if(x/y < 0){
		a = pow(a*a + y, 3);
		cout << "(f(x)^2 + y)^3\n";
	}
	else if(x/y > 0){
		a = log(a/y)+x/y;
		cout << "ln(f(x)/y)+x/y\n";
	}
	else{
		a = pow(fabs(sin(y)), 1.0/3);
		cout << "|sin(y)|^(1/3)\n";
	}
	cout << a;
	return 0;
}