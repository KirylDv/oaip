#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

/// Ассимптотика О(mnlog(n))

bool comp(const vector<int> & a, const vector<int> & b)
{
	long long A, B;
	A = 0; B = 0;
	for(int i = 0; i < a.size(); i++)
	{
		A += a[i];
		B += b[i];
	}
	if(A < B) return 1;
	return 0;
}

void bubble_sort(vector<vector<int >> & a){
	for (int i = 0; i < a.size()-1; ++i)
		for(int j = i+1; j < a.size(); j++)
			if(!comp(a[i], a[j])) swap(a[i], a[j]);
}

int main()
{
	int n, m, i, j;
	cout << "Vvedite n, m\n";
	cin >> n >> m;
	vector<vector<int> > a(n, vector<int>(m) );
	for(j = 0; j < n; j++)
		for(i = 0; i < m; i++)
			cin >> a[j][i];
	bubble_sort(a);
	cout << '\n';
	for(j = 0; j < n; j++)
	{
		for(i = 0; i < m; i++)
			cout << a[j][i] << ' ';
		cout << '\n';
	}
	return 0;
}